package com.llb.sio.slam.suivivisitegsb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class AfficheListeVisites extends Activity {

    private ListView listView;
	private List<Visite> listeVisites;
    private Modele modele = Modele.getModele();// A compléter

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liste_visite);

        // Affichage de la liste des visites
        listeVisites = modele.listeVisites();
        Log.d("liste","ListeVisirtes : "+listeVisites);
        listView = (ListView) findViewById(R.id.ListeVisites);
        VisiteAdapter adaptVisite = new VisiteAdapter(this, listeVisites);
        listView.setAdapter(adaptVisite);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Toast.makeText(getApplicationContext(), "Choix : " + listeVisites.get(position).getIdRapport(), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(AfficheListeVisites.this, AfficheVisite.class);
                i.putExtra("paramIdVisite", listeVisites.get(position).getIdRapport() );
                startActivity(i);
                // Appel de l'activité AfficheVisite
                // A compléter

            }
        });
    }
}

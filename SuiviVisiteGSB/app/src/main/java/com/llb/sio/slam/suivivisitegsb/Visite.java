package com.llb.sio.slam.suivivisitegsb;

import java.util.Date;


public class Visite {


    //Données ne pouvant être modifiées
    private String id;
    private String nom;
    private String prenom;
    private String adresse;
    private String tel;
    private String idVisiteur;



    private String idRapport;
    // Données à saisir lors de la visite
    private Boolean present;



    private Date date;

    private String motif;
    private float niveauConfiance;
    private String lisibilite; // Lisibilité des notices
    private String bilan;

    public Visite() {
    }

    public Visite(String id, String nom, String prenom, String adresse, String tel){
        this.present = false;
        this.date = new Date();
        this.motif = "";
        this.niveauConfiance = 0;
        this.lisibilite = "";
        this.bilan = "";
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.tel = tel;
    }

    public String getIdRapport() { return idRapport; }

    public void setIdRapport(String idRapport) { this.idRapport = idRapport; }

    public String getIdVisiteur() { return idVisiteur; }

    public void setIdVisiteur(String idVisiteur) { this.idVisiteur = idVisiteur; }

    public String getId() { return id; }

    public String getNom() { return nom; }

    public void setNom(String nom) {this.nom = nom;}

    public String getPrenom() { return prenom; }

    public void setPrenom(String prenom) {this.prenom = prenom;}

    public String getAdresse() { return adresse; }

    public void setAdresse(String adresse) {this.adresse = adresse;}

    public String getTel() { return tel; }

    public void setTel(String tel) {this.tel = tel;}

    public void setId(String id) { this.id = id; }

    public Boolean getPresent() {
        return present;
    }

    public void setPresent(Boolean present) {
        this.present = present;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public float getNiveauConfiance() {
        return niveauConfiance;
    }

    public void setNiveauConfiance(float niveauConfiance) {
        this.niveauConfiance = niveauConfiance;
    }

    public String getLisibilite() {
        return lisibilite;
    }

    public void setLisibilite(String lisibilite) {
        this.lisibilite = lisibilite;
    }

    public String getBilan() {
        return bilan;
    }

    public void setBilan(String bilan) {
        this.bilan = bilan;
    }

    @Override
    public String toString() {
        return "Visite{" +
                "id='" + id + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", tel='" + tel + '\'' +
                ", present=" + present +
                ", date=" + date +
                ", motif='" + motif + '\'' +
                ", niveauConfiance=" + niveauConfiance +
                ", lisibilite='" + lisibilite + '\'' +
                ", bilan='" + bilan + '\'' +
                '}';
    }
    public void copieVisite(Visite v){
        Visite copieValVisite = new Visite(v.id, v.nom, v.prenom, v.adresse, v.tel);
    }

}

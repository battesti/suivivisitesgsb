package com.llb.sio.slam.suivivisitegsb;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    private Button btnConnexion, btnAnnuler;
    private EditText editTextLogin, editTextPassword;
    private String identifiant, mdp;
    private AsyncTask<String,String,Boolean> connexionAsynchrone;
    private SharedPreferences saveDataAuth;
    private CheckBox check_remember;
    SharedPreferences.Editor editor;
    public static final String MyPREFERENCES = "DataAuth";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        saveDataAuth = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = saveDataAuth.edit();
        check_remember = (CheckBox) findViewById(R.id.chk_b_remeber);

        editTextLogin = (EditText) findViewById(R.id.etLogin);
        editTextPassword = (EditText) findViewById(R.id.etPassword);

        btnConnexion = (Button) findViewById(R.id.btnConnexion);
        btnAnnuler = (Button) findViewById(R.id.btnAnnuler);
        btnAnnuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        btnConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                identifiant = editTextLogin.getText().toString();
                mdp = editTextPassword.getText().toString();
                //String[] mesParams = {identifiant, mdp, "http://gsb.labo.ch:8085/suivivisitesgsb/authentification.php"};
                //String[] mesParams = {identifiant, mdp, "http://192.168.60.101/suivivisitesgsb/authentification.php"};
                String[] mesParams = {identifiant, mdp, "http://193.248.44.31:8085/suivivisitesgsb/authentification.php"};
                connexionAsynchrone = new Connexion(Login.this).execute(mesParams);
            }
        });

        String loginSaved = saveDataAuth.getString("saveLogin",null);
        String PasswordSaved = saveDataAuth.getString("savePassword",null);
        Boolean RemeberBoxSaved = saveDataAuth.getBoolean("saveBoolCheck",false);

        if (loginSaved != null && PasswordSaved != null && RemeberBoxSaved ){
            editTextLogin.setText(loginSaved);
            editTextPassword.setText(PasswordSaved);
            check_remember.setChecked(true);
        } else{
            editTextLogin.setText("");
            editTextPassword.setText("");
            check_remember.setChecked(false);
        }

    }
    // --> Retour des données du serveur
    // Méthode éxécutée après l'AsyncTask Connexion
    public void retourVersAuthentification(StringBuilder codeRetour) {
        // A compléter
        // Authentification réussie si la valeur de retour est 1 (voir script PHP)
        if (codeRetour.toString().equals("1")){
            Toast.makeText(this, "Authentification réussie", Toast.LENGTH_SHORT).show();
            if(check_remember.isChecked()){
                editor.putString("saveLogin",editTextLogin.getText().toString()).putString("savePassword",editTextPassword.getText().toString()).putBoolean("saveBoolCheck",true).putBoolean("saveConnected",true);

                editor.apply();

                //SELECT * FROM `visiteur` JOIN rapport ON idVisiteur = visiteur.id WHERE visiteur.id ="a55"
                //Affichage des liste uniquement pour cette utilisateur.
            } else{
                editor.putBoolean("saveBoolCheck",false);
                editor.putString("saveLogin",editTextLogin.getText().toString()).putString("savePassword",editTextPassword.getText().toString());
                editor.putBoolean("saveConnected",true);
                editor.apply();
            }
            setResult(Activity.RESULT_OK);
            finish();
        } else {
            // Echec Authentification
            Toast.makeText(this, "Echec authentification", Toast.LENGTH_SHORT).show();
            editor.putBoolean("saveBoolCheck",false);
            editor.putBoolean("saveConnected",false);
            editor.apply();
            editTextPassword.setText("");
            setResult (Activity.RESULT_CANCELED);
            finish();
        }
    }
}

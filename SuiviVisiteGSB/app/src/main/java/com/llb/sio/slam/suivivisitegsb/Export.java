package com.llb.sio.slam.suivivisitegsb;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;

/**
 * Created by btssio2 on 25/11/16.
 */
public class Export extends AppCompatActivity {

    private Button btnImporter, btnRetourImport;
    private AsyncTask<String, String, Boolean> connexionAsynchrone;
    Context context;
    ProgressBar waitingCirlcle;
    public void envoieData(String login, String mdp, Context contextAppelan){
        context = contextAppelan;
        waitingCirlcle = (ProgressBar)  ((Activity)context).findViewById(R.id.waitingCircle);
        waitingCirlcle.setVisibility(View.VISIBLE);

        String chaineJson = new Gson().toJson(Modele.listeVisites());
        //String[] mesParams = {login, mdp, "http://192.168.60.101/suivivisitesgsb/export.php",chaineJson};
        String[] mesParams = {login, mdp, "http://193.248.44.31:8081/suivivisitesgsb/export.php",chaineJson};
        //String[] mesParams = {login, mdp, "http://192.168.1.22/apigsb/export.php",chaineJson};
        connexionAsynchrone = new Connexion(Export.this).execute(mesParams);

        // RÈcupËre les boutons depuis le layout
        // PAS a complÈter

        // Gestion de l'ÈvËnement sur le clic du bouton Retour
        // PAS a complÈter

        // Gestion de l'ÈvËnement sur le clic du bouton Importer
        // PAS a complÈter
    }
    // --> Retour
    // MÈthode exÈcutÈe aprËs la fin de l'AsynTask
    public void retourExport(StringBuilder codeRetour) {
        if (codeRetour.toString().equals("1")){
            Toast.makeText(context, "Exportation réussie.", Toast.LENGTH_SHORT).show();
            waitingCirlcle.setVisibility(View.INVISIBLE);
            setResult(Activity.RESULT_OK);
        } else {
            Toast.makeText(context, "Echec de l'exportation.", Toast.LENGTH_SHORT).show();
            setResult (Activity.RESULT_CANCELED);
        }
    }
}

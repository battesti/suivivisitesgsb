package com.llb.sio.slam.suivivisitegsb;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Connexion extends AsyncTask<String, String, Boolean> {

    // Référence à l'activité qui appelle
    private WeakReference<Activity> activiteAppelante = null;
    private String nomClasseActiviteAppelante;
    private StringBuilder stringBuilder = new StringBuilder();

    // Constructeur
    public Connexion(Activity activiteAppelante) {
        this.activiteAppelante = new WeakReference<Activity>(activiteAppelante);// Référence à l'activité appelante
        nomClasseActiviteAppelante = activiteAppelante.getClass().toString();	// Nom de la classe (activité) appelante
    }

    // Méthode exécutée au démarrage
    @Override
    protected void onPreExecute() {
        // Au lancement, on envoie un message à l'appelant
        //if (activiteAppelante.get() != null)
            //Toast.makeText(activiteAppelante.get(), "AsyncTask démarre", Toast.LENGTH_SHORT).show();
    }

    // Méthode exécutée en arrière plan
    @Override
    protected Boolean doInBackground(String... params){
        String identifiant = "", mdp = "", urlServiceGSB = "", chaineJson ="";

        // Affectation des paramètres adéquats en fonction de l'activité appelante
        if (nomClasseActiviteAppelante.contains("Login")
                || nomClasseActiviteAppelante.contains("Import")
                || nomClasseActiviteAppelante.contains("Export") ) {
            identifiant = params[0];
            mdp = SHA1(params[1]);
            urlServiceGSB = params[2];
            if(nomClasseActiviteAppelante.contains("Export")) {
                chaineJson = params[3];
            }
        }

        // Paramètres utiles à l'activité Exportation
        // A compléter

        // Connexion au serveur en POST et envoi des données d'authentification au format JSON
        HttpURLConnection urlConnexion = null;

        try {
            URL url = new URL(urlServiceGSB);
            urlConnexion = (HttpURLConnection) url.openConnection();
            urlConnexion.setRequestProperty("Content-Type", "application/json");
            urlConnexion.setRequestProperty("Accept", "application/json");
            urlConnexion.setRequestMethod("POST");
            urlConnexion.setDoOutput(true);
            urlConnexion.setConnectTimeout(5000);

            OutputStreamWriter out = new OutputStreamWriter(urlConnexion.getOutputStream(), "UTF-8");

            // Selon l'activité appelante on peut passer des paramètres en JSON
            // ***************** LOGIN ***************** //
            if (nomClasseActiviteAppelante.contains("Login")) {
                // Création objet JSON clé->valeur pour l'activité Authentification
                JSONObject jsonParam = new JSONObject();
                jsonParam.put("login", identifiant);
                jsonParam.put("mdp", mdp);
                out.write(jsonParam.toString());
                out.flush();
                Log.i("Login", jsonParam.toString());
            }
            // ***************** IMPORT ***************** //
            if (nomClasseActiviteAppelante.contains("Import")) {
                // Création objet JSON clé->valeur pour l'activité Authentification
                JSONObject jsonParam = new JSONObject();
                jsonParam.put("login", identifiant);
                jsonParam.put("mdp", mdp);
                out.write(jsonParam.toString());
                out.flush();
                Log.i("import", jsonParam.toString());
            }
            // ***************** EXPORT ***************** //
            if (nomClasseActiviteAppelante.contains("Export")) {
                // Création objet JSON clé->valeur pour l'activité Authentification
                JSONObject jsonParam = new JSONObject();
                jsonParam.put("jsonVisites", chaineJson);
                out.write(jsonParam.toString());
                out.flush();
                int maxLogSize = 1000;
                for(int i = 0; i <= chaineJson.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i+1) * maxLogSize;
                    end = end > chaineJson.length() ? chaineJson.length() : end;
                    Log.v("json", chaineJson.substring(start, end));
                }
            }

            // Création objet JSON clé->valeur pour l'activité Export
            // A compléter

            out.close();

            // Traitement du script PHP correspondant sur le serveur

            // Récupération du résultat de la requête au format JSON depuis le serveur
            int HttpResult = urlConnexion.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnexion.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    stringBuilder.append(line);
                }

                br.close();
                /*int maxLogSize = 1000;
                for(int i = 0; i <= chaineJson.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i+1) * maxLogSize;
                    end = end > chaineJson.length() ? chaineJson.length() : end;
                    Log.v("retourServeur", "Reçu du serveur :" + stringBuilder.substring(start, end));
                }*/
                Log.i("retourServeur", "Reçu du serveur :" + stringBuilder);

            } else
                Log.i("retourServeur", "Erreur :" + urlConnexion.getResponseMessage());

            // Gestion des exceptions pouvant survenir pendant la connexion vers le serveur
        } catch (MalformedURLException e) {
            Toast.makeText(activiteAppelante.get(), "URLExeption : ", Toast.LENGTH_SHORT).show();
            return false;

        } catch (IOException e) {
            Toast.makeText(activiteAppelante.get(), "IOExeption : ", Toast.LENGTH_SHORT).show();
            return false;

        } catch (JSONException e){
            Toast.makeText(activiteAppelante.get(), "JSONExeption : ", Toast.LENGTH_SHORT).show();
            return false;


    } finally {
        // Ferme la connexion vers le serveur dans tous les cas
        if (urlConnexion != null)
            urlConnexion.disconnect();
    }

    return true;
}

    // Utilisation de onProgress pour afficher des messages pendant le doInBackground
    @Override
    protected void onProgressUpdate(String... param) {
    }

    // Méthode exécutée à la fin de la méthode doInBackGroud()
    @Override
    protected void onPostExecute(Boolean result) {
        if (activiteAppelante.get() != null) {
            if (result){
               // Toast.makeText(activiteAppelante.get(), "Fin AsynckTask", Toast.LENGTH_SHORT).show();

                // Retour vers une méthode de la classe appelante
                if (nomClasseActiviteAppelante.contains("Login")) {
                    ((Login) activiteAppelante.get()).retourVersAuthentification(stringBuilder);
                }
                if (nomClasseActiviteAppelante.contains("Import")) {
                    ((Import) activiteAppelante.get()).retourImport(stringBuilder);
                }
                if (nomClasseActiviteAppelante.contains("Export")) {
                    ((Export) activiteAppelante.get()).retourExport(stringBuilder);
                }
                // A compléter pour les activités Import et Export

            } else
                Toast.makeText(activiteAppelante.get(), "Fin ko", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCancelled() {
        if (activiteAppelante.get() != null)
            Toast.makeText(activiteAppelante.get(), "Annulation", Toast.LENGTH_SHORT).show();
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String SHA1(String text){
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] textBytes = text.getBytes("iso-8859-1");
            md.update(textBytes, 0, textBytes.length);
            byte[] sha1hash = md.digest();
            return convertToHex(sha1hash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
}


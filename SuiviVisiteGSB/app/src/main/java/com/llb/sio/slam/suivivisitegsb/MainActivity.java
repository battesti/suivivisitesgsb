package com.llb.sio.slam.suivivisitegsb;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_WRITE_STORAGE = 1;
    String TAG= "--------------------";
    TextView welcome;
    //POur savoir si l'utilisateur est connecté
    private String identifiant, mdp;
    private Boolean connected;
    private SharedPreferences saveDataAuth;
    SharedPreferences.Editor editor;
    private AsyncTask<String,String,Boolean> connexionAsynchrone;
    public static final String MyPREFERENCES = "DataAuth";
    ProgressBar waitingCirlcle;
    Button deconnexion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView authentification = (ImageView) findViewById(R.id.imgAuth);
        ImageView visite = (ImageView) findViewById(R.id.imgVisite);
        ImageView telechargement = (ImageView) findViewById(R.id.imgDownload);
        ImageView export = (ImageView) findViewById(R.id.imgUpload);
        deconnexion = (Button) findViewById(R.id.btDeconnexion);
        welcome = (TextView) findViewById(R.id.txt_welcome);
        waitingCirlcle = (ProgressBar)  findViewById(R.id.waitingCircle);
        waitingCirlcle.setVisibility(View.INVISIBLE);
        authentification.setOnClickListener(imageClick);
        visite.setOnClickListener(imageClick);
        telechargement.setOnClickListener(imageClick);
        export.setOnClickListener(imageClick);
        deconnexion.setOnClickListener(imageClick);

        saveDataAuth = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = saveDataAuth.edit();
        if(saveDataAuth.getBoolean("saveConnected",false)){
            welcome.setText("Bienvenue "+saveDataAuth.getString("saveLogin",null)+" !");
        }else{
            deconnexion.setVisibility(View.INVISIBLE);
        }

        //Demande de Permission :
        if(isStoragePermissionGranted()){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }

    }

//Permission de stockage :
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {
                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }

//Idem
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.d("--------------------","Methode résult");
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED) {
                    Log.v(TAG,"Permission: "+permissions[0]+ " was "+grantResults[0]);

                } else {
                    Log.d("--------------------","Else dans methode résult");
                        // Permission refusée (à compléter plus tard)
                }
            }
        }
    }

    // gestion des onclick des buttons.
    private View.OnClickListener imageClick = new View.OnClickListener() {
        public void onClick(View v) {
            Intent i;
            Boolean connected = saveDataAuth.getBoolean("saveConnected",false);
            ConnexionInternet co = new ConnexionInternet();
            switch (v.getId()) {
                // Cas du clic sur l'image Visite
                case R.id.imgVisite:
                    if(connected || !co.isConnected(MainActivity.this)) {
                        i = new Intent(MainActivity.this, AfficheListeVisites.class);
                        startActivity(i);
                    }else
                        Toast.makeText(getApplicationContext(), "Veuillez vous connectez pour accéder a ce contenue !", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.imgAuth:
                    if(!connected && co.isConnected(MainActivity.this)) {
                        i = new Intent(MainActivity.this, Login.class);
                        startActivity(i);
                    }else {
                        if(connected){
                            Toast.makeText(getApplicationContext(), "Vous êtes déjà connecté !", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(), "Vous devez avoir une connexion internet pour vous connecter !", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case R.id.imgUpload:
                    if(connected && co.isConnected(MainActivity.this)) {
                        final Export anExport = new Export();
                        new AlertDialog.Builder(MainActivity.this)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Confirmation")
                                .setMessage("Etes vous sur de vouloir importer les nouvelles données ?")
                                .setPositiveButton("Oui", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        anExport.envoieData(saveDataAuth.getString("saveLogin",null), saveDataAuth.getString("savePassword",null),MainActivity.this);
                                    }

                                })
                                .setNegativeButton("Non", null)
                                .show();
                    }else {
                        if(!connected){
                            Toast.makeText(getApplicationContext(), "Vous devez être connecté !", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(), "Vous devez avoir une connexion internet pour vous connecter !", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case R.id.imgDownload:

                    if(connected && co.isConnected(MainActivity.this)) {
                        final Import anImport = new Import();
                        new AlertDialog.Builder(MainActivity.this)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle("Confirmation")
                                .setMessage("Etes vous sur de vouloir importer les nouvelles données ?")
                                .setPositiveButton("Oui", new DialogInterface.OnClickListener()
                                {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        anImport.demandeData(saveDataAuth.getString("saveLogin",null), saveDataAuth.getString("savePassword",null),MainActivity.this);
                                    }

                                })
                                .setNegativeButton("Non", null)
                                .show();
                    }else {
                        if(!connected){
                            Toast.makeText(getApplicationContext(), "Vous devez être connecté !", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(), "Vous devez avoir une connexion internet pour vous connecter !", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case R.id.btDeconnexion:
                    Toast.makeText(getApplicationContext(), "Deco", Toast.LENGTH_SHORT).show();
                    editor.putBoolean("saveBoolCheck",false);
                    editor.putBoolean("saveConnected",false);
                    editor.putString("saveLogin",null).putString("savePassword",null).putBoolean("saveBoolCheck",true);
                    editor.apply();
                    welcome.setText("");
                    deconnexion.setVisibility(View.INVISIBLE);
                    Modele.deleteVisites();
                    break;
            }
        }};

}


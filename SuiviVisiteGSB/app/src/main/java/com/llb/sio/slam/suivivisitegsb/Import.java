package com.llb.sio.slam.suivivisitegsb;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Import extends AppCompatActivity {

    private Button btnImporter, btnRetourImport;
    private AsyncTask<String, String, Boolean> connexionAsynchrone;
    Context context;
    ProgressBar waitingCirlcle;
    public void demandeData(String login, String mdp, Context contextAppelan){
        context = contextAppelan;
        waitingCirlcle = (ProgressBar)  ((Activity)context).findViewById(R.id.waitingCircle);
        waitingCirlcle.setVisibility(View.VISIBLE);
        //String[] mesParams = {login, mdp, "http://192.168.60.101/suivivisitesgsb/import.php"};
        String[] mesParams = {login, mdp, "http://193.248.44.31:8081/suivivisitesgsb/import.php"};
        //String[] mesParams = {login, mdp, "http://192.168.1.22/apigsb/import.php"};
        connexionAsynchrone = new Connexion(Import.this).execute(mesParams);
    }
    // --> Retour
    // MÈthode exÈcutÈe aprËs la fin de l'AsynTask
    public void retourImport(StringBuilder donneesJSONServeur) {
        ArrayList<Visite> listeVisites = new ArrayList<Visite>();

        // RÈcupÈration des ÈlÈments au format JSON (dans un tableau)
        JsonElement json = new JsonParser().parse(donneesJSONServeur.toString());
        JsonArray tabJSON = json.getAsJsonArray();

        for(int i=0; i < tabJSON.size();i++){
            JsonObject jsonObject = tabJSON.get(i).getAsJsonObject();
            listeVisites.add(createVisite(jsonObject));
        }
        String aaaajson = new Gson().toJson(listeVisites);
        // Si la liste contient visites alors on la sauvegarde dans la base DB4o
        if (!listeVisites.isEmpty()) {
            Modele.deleteVisites();
            Modele.addVisites(listeVisites);
            Toast.makeText(context, "Importation réussie", Toast.LENGTH_SHORT).show();
            waitingCirlcle.setVisibility(View.INVISIBLE);
            setResult (Activity.RESULT_OK);
        } else {
            Toast.makeText(context, "Echec de l'importation", Toast.LENGTH_SHORT).show();
            setResult (Activity.RESULT_CANCELED);
        }
    }
    public Visite createVisite(JsonObject jsonObject){
        Visite visite = new Visite();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        visite.setMotif(visiteAtribus(jsonObject, "motif"));
        visite.setPresent(true);
        visite.setLisibilite(visiteAtribus(jsonObject, "lisibiliteNotice"));
        visite.setBilan(visiteAtribus(jsonObject, "bilan"));
        visite.setNiveauConfiance(Float.parseFloat(visiteAtribus(jsonObject, "niveauConfiance")));
        visite.setNom(visiteAtribus(jsonObject, "nom"));
        visite.setPrenom(visiteAtribus(jsonObject, "prenom"));
        visite.setId(visiteAtribus(jsonObject, "idMedecin"));
        visite.setAdresse(visiteAtribus(jsonObject, "adresse"));
        visite.setTel(visiteAtribus(jsonObject, "tel"));
        visite.setIdVisiteur(visiteAtribus(jsonObject, "idVisiteur"));
        visite.setIdRapport(visiteAtribus(jsonObject, "id"));
        try{
            visite.setDate(dateFormat.parse(visiteAtribus(jsonObject, "date")));
        }catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d("-----add-----",visite.toString());
        return visite;
    }
    public String visiteAtribus(JsonObject jsonObject, String variableWanted){
        JsonElement jsonVar = jsonObject.get(variableWanted);
        Log.d("var", variableWanted + " = " + jsonVar.getAsString());
        String attribus = jsonVar.getAsString();
        return attribus;
    }
}
